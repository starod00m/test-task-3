import re

def count_words(file_name):
    with open(file_name, 'r', encoding='UTF-8') as f:
        print('Введите слово для поиска (регистр не важен)')
        word = input().lower()
        input_string = f.read().lower()
        count = len(re.findall(r'\b%s\b' % re.escape(word), input_string))
        print(f'Кол-во вхождений слова "{word}" в файл "{file_name}" равно {count}')

while True:
    print('Введите название файла (для выхода ничего не вводите и нажмите нажмите Enter)')
    file_name = input()
    if file_name != '': # условие выхода из цикла
        try:
            count_words(file_name)
            break
        except FileNotFoundError:
            print('Файл не найден, повторите ввод')
            continue
    else: break